import {sendName} from "./send-name";
import {getHistory} from "./get-history";
import {getNickname, setNickname} from "./change-name";

const hidden = document.querySelector('.header');
const container = document.getElementById("containerEntry");
const nameBlock = document.getElementById("nickname");
const errorBlock = document.getElementById("errorEntry");

export function authenticateUser() {
	const name = document.getElementById("nicknameEntry").value;
	const password = document.getElementById("passwordEntry").value;

	const xhr = new XMLHttpRequest();
	xhr.open("POST", "/authenticate-user", true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.onload = function () {
		if (xhr.status != 200) {
			alert(`Error ${xhr.status}: ${xhr.statusText}`);
		}
	};

	xhr.send(JSON.stringify({
		status: true,
		nickname : name,
		password : password
	}));

	xhr.onload = function () {
		try {
			const response =  JSON.parse(xhr.responseText);
			if(response.status){
				hidden.style.display = "flex";
				nameBlock.placeholder = name;
				container.style.display = "none";
				errorBlock.style.display = "none";

				getHistory(name);
				setNickname(name);

				setInterval(function () {
					sendName(getNickname());
				 }, 5000)

			} else {
				errorBlock.style.display = "block";
				errorBlock.innerHTML = "Пользователь с таким именем и паролем не найден";
			}

		} catch (e) {
			console.log(e);
		}
	};
}