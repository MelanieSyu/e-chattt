import {hideWindow} from "./modal-window";

const errorBlock = document.getElementById("error");
const nameBlock = document.getElementById("nickname");

let nickname;

export function changeName() {
	const name = document.getElementById("nickname").value;

	const xhr = new XMLHttpRequest();
	xhr.open("POST", "/change-name", true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.onload = function () {
		if (xhr.status != 200) {
			alert(`Error ${xhr.status}: ${xhr.statusText}`);
		}
	};

	xhr.send(JSON.stringify({
		newNickname: name,
		oldNickname: getNickname()
	}));

	xhr.onload = function () {
		try {
			const response = JSON.parse(xhr.responseText);
			if (response.status) {
				nameBlock.placeholder = name;
				errorBlock.style.display = "none";

				hideWindow();
				setNickname(name);
			} else {
				errorBlock.style.display = "block";
				errorBlock.innerHTML = "*Это имя занято другим пользователем: " + response.name;
			}

		} catch (e) {
			console.log(e);
		}
	};
}

export function getNickname() {
	return nickname;
}

export function setNickname(name) {
	nickname = name;
}
