import {drawMessage} from "./draw-message";
import {getNickname} from "./change-name";

let count = 1;
const  history = document.getElementById("messageHistory");

export function loadHistory() {
	const xhr = new XMLHttpRequest();
	xhr.open("POST", "/load-history", true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.onload = function () {
		if (xhr.status != 200) {
			alert(`Error ${xhr.status}: ${xhr.statusText}`);
		}
	};

	xhr.send(JSON.stringify({count: ++count}));

	xhr.onload = function () {
		try {
			let name = getNickname();
			let response = JSON.parse(xhr.responseText);
			const scrollFromButton = history.scrollHeight;

			response.docs = response.docs.reverse();
			response.docs.map(message => {
				if (name != message.nickname) {
					drawMessage(false, message, true);
				} else {
					drawMessage(true, message, true);
				}
			});

			history.scrollTop = history.scrollHeight - scrollFromButton;

		}catch (e) {
			console.log(e);  
		}
	};
}