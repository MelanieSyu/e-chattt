import {sendName} from "./send-name";
import {getHistory} from "./get-history";
import {getNickname, setNickname} from "./change-name";

const hidden = document.querySelector('.header');
const container = document.getElementById("containerEntry");
const nameBlock = document.getElementById("nickname");
const errorBlock = document.getElementById("errorReg");

export function signUpUser() {
	const name = document.getElementById("nicknameReg").value;
	const password = document.getElementById("passwordReg").value;

	const xhr = new XMLHttpRequest();
	xhr.open("POST", "/signup-user", true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.onload = function () {
		if (xhr.status != 200) {
			alert(`Error ${xhr.status}: ${xhr.statusText}`);
		}
	};

	xhr.send(JSON.stringify({
		status: true,
		nickname : name,
		password : password
	}));

	xhr.onload = function () {
		try {
			const response =  JSON.parse(xhr.responseText);
			if(response.status){
				hidden.style.display = "flex";
				nameBlock.placeholder = name;
				container.style.display = "none";
				errorBlock.style.display = "none";

				setNickname(name);
				getHistory(name);

				setInterval(function () {
					sendName(getNickname());
				}, 5000)
			} else {
				errorBlock.style.display = "block";
				errorBlock.innerHTML = "Это имя занято другим пользователем: " + response.nickname;
			}
		} catch (e) {
			console.log(e);
		}
	};
}