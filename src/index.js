import "./js/modal-window";
import './styles/style.less';
import {signUpUser} from "./js/sign-up";
import {validation} from "./js/validation";
import {authenticateUser} from "./js/auth";
import {initFields} from "./js/checkButton";
import {loadHistory} from "./js/load-history";
import {init_users_list} from "./js/users-list.js";
import {changeName, getNickname} from "./js/change-name";
import {drawMessage, drawList, drawIp} from "./js/draw-message";
import {sendButton, hideWindow, toggleForWindow, variablesForModal, entryButton, signupButton} from "./js/modal-window";

const reg = document.getElementById("reg");
const enter = document.getElementById("enter");
const history = document.getElementById("messageHistory");
const modalReg = document.getElementById("modalRegWindow");
const modalEntry = document.getElementById("modalEntryWindow");
const menuUsersList = document.getElementById("menuUsersList");

reg.addEventListener("click", function () {
	modalEntry.style.display = "none";
	modalReg.style.display = "block";
});

enter.addEventListener("click", function () {
	modalEntry.style.display = "block";
	modalReg.style.display = "none";
});

history.addEventListener("scroll", function () {
	if (history.scrollTop == 0) {
		loadHistory();
	}
});

entryButton.addEventListener('click', function () {
	const name = document.getElementById("nicknameEntry").value;
	const error = document.getElementById("errorEntry");
	const password = document.getElementById("passwordEntry").value;

	if (validation(name, password, error)) {
		error.style.display = "none";
		authenticateUser();
	}
});

signupButton.addEventListener('click', function () {
	const name = document.getElementById("nicknameReg").value;
	const error = document.getElementById("errorReg");
	const password = document.getElementById("passwordReg").value;

	if (validation(name, password, error)) {
		error.style.display = "none";
		signUpUser();
	}
});

menuUsersList.addEventListener("click", function () {
	init_users_list();
});

document.addEventListener("DOMContentLoaded", function () {
	initFields();
});

sendButton.sendButton.addEventListener('click', function () {
	sendMessage();
});

variablesForModal.overlay.addEventListener('click', function () {
	hideWindow();
});

variablesForModal.closeButton.addEventListener('click', function () {
	toggleForWindow();
});

variablesForModal.settingsButton.addEventListener('click', function () {
	toggleForWindow();
});

variablesForModal.setNickNameButton.addEventListener('click', function () {
	const name = document.getElementById("nickname").value;
	const error = document.getElementById("error");
	const password = document.getElementById("passwordEntry").value;
	const password1 = document.getElementById("passwordReg").value;

	if (validation(name, password.length != 0 ? password:password1, error)) {
		error.style.display = "none";
		changeName();
	}
});

//const ws = new WebSocket('ws://e-chat-com.eu-4.evennode.com/');

const ws = new WebSocket('ws://localhost:3001');

const input = document.getElementById('inputField');

input.onkeypress = e => {
	if(e.keyCode == 13 && !e.shiftKey) {
		sendMessage();
	}
}

ws.onopen = () => {
};

ws.onclose = () => {
};

function sendMessage() {
	let newMessage = sendButton.inputField.value;
	if (newMessage === "" || !newMessage.replace(/\s/g, '').length) return -1;
	const data = {
		nickname: getNickname(),
		message: newMessage.trim(),
		createdAt: new Date().toUTCString()
	};
	sendButton.inputField.value = "";
	drawMessage(true, data);
	ws.send(JSON.stringify(data));
}

ws.onmessage = response => {
	try {
		const data = JSON.parse(response.data);
		if (data.forList == true) {
			drawList(data);
		} else if (data.forModal == true) {
			drawIp(data);
		} else {
			drawMessage(false, data);
		}
	} catch (e) {
		console.log("Ошибка: " + e)
	}
};